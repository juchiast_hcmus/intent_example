package com.example.qwe.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class Main2Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        float a = intent.getFloatExtra("a", 0);
        float b = intent.getFloatExtra("b", 0);
        solve(a, b);
        finish();
    }

    private void solve(float a, float b) {
        if (a == 0) {
            if (b == 0) {
                setResult(1);
            } else {
                setResult(0);
            }
        } else {
            Intent res = new Intent();
            res.putExtra("result", -b/a);
            setResult(2, res);
        }
    }
}
