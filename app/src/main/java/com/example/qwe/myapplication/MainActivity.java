package com.example.qwe.myapplication;

import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onSolve(View view) {
        try {
            float a = getFloatFromEditText(R.id.a);
            float b = getFloatFromEditText(R.id.b);
            solve(a, b);
        } catch (Exception e) {
            setResultText("Invalid string");
        }
    }

    private float getFloatFromEditText(int id) {
        return Float.parseFloat(((EditText) findViewById(id)).getText().toString());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == 12345) {
            if (resultCode == 0) {
                setResultText("No solution");
            } else if (resultCode == 1) {
                setResultText("Too many solutions");
            } else {
                displaySolution(data);
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void displaySolution(Intent data) {
        Float f = data.getFloatExtra("result", 0);
        setResultText("x = " + f);
    }

    private void setResultText(String s) {
        ((TextView)findViewById(R.id.result)).setText(s);
    }

    private void solve(float a, float b) {
        Intent intent = new Intent(this, Main2Activity.class);
        intent.putExtra("a", a);
        intent.putExtra("b", b);
        startActivityForResult(intent, 12345);
    }
}
